const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = () => {
    const plugins = [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
        }),
    ];

    return {
        entry: './src/index.js',
        devServer: {
            historyApiFallback: true,
        },
        output: {
            path: path.join(__dirname, '/dist'),
            filename: "index_bundle.js",
            publicPath: '/'
        },
        resolve: {
            alias: {
                importName: 'src',
                '%app': 'src'
            },
            extensions: ['.js', '.jsx'],
            modules: ['node_modules']
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            "@babel/preset-env",
                            "@babel/preset-react",
                        ],
                        plugins: [
                            "@babel/plugin-proposal-class-properties"
                        ]
                    }
                },
            ]
        },
        plugins: plugins,
    }
};