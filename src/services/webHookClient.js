import axios from 'axios';

const getWebHookListByFilter = (filter) => {
    return axios.get(
        `http://subscription-api/web-hooks/rest/v1/web-hooks`,
        {
            params: filter,
        },
    );
};

const processWebHook = (hash) => {
    return axios.put(`http://subscription-api/web-hooks/rest/v1/web-hook/process/${hash}`)
};

export {
    getWebHookListByFilter,
    processWebHook
}
