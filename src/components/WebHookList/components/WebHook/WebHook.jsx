import React, { useState, useEffect } from 'react';
import PropType from 'prop-types';
import { processWebHook } from '../../../../services/webHookClient';

function WebHook({ webHook, removeProcessedWebHook }) {
    const [processWebHookSubmitted, setProcessWebHookSubmitted] = useState(false);

    useEffect(
        () => {
            if (!processWebHookSubmitted) {
                return;
            }

            (async () => {
                try {
                    await processWebHook(webHook.hash);
                    removeProcessedWebHook(webHook.hash)
                } catch (error) {
                    //snackbar
                }

            })();
        },
        [processWebHookSubmitted],
    );

    return (
        <li style={{ marginBottom: 20 }}>
            {`${webHook.provider} - ${webHook.hash}`}
            <button
                onClick={() => setProcessWebHookSubmitted(true)}
                style={{ marginLeft: 20 }} // hack
            >
                process web hook
            </button>
        </li>
    )
}

WebHook.propTypes = {
    webHook: PropType.shape({
        hash: PropType.string.isRequired,
        provider: PropType.string.isRequired,
    }).isRequired,
    removeProcessedWebHook: PropType.func.isRequired,
};

export default WebHook;
