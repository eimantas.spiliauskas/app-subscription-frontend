import React, { useState, useEffect } from 'react';
import WebHook from './components/WebHook';
import { getWebHookListByFilter } from '../../services/webHookClient';

function WebHookList() {
    const [webHooks, setWebHooks] = useState(null);

    const removeWebHookFromList = (hash) => setWebHooks(webHooks.filter(webHook => webHook.hash !== hash));

    useEffect(
        () => {
            (async () => {
                try {
                    const result = await getWebHookListByFilter({
                        limit: 20,
                        status: 'new'
                    });

                    setWebHooks(result.data.items);
                } catch (error) {
                    // snackbar about error
                }
            })()
        },
        [],
    );

    if (webHooks === null) {
        return null;
    }

    if (webHooks.length === null) {
        // empty state
        return <div>not found</div>
    }

    return (
        <ul>
            {
                webHooks.map(webHook => (
                    <WebHook
                        key={webHook.hash}
                        webHook={webHook}
                        removeProcessedWebHook={removeWebHookFromList}
                    />
                ))
            }
        </ul>
    );
}

export default WebHookList;