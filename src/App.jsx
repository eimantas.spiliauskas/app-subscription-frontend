import React from 'react';
import WebHookList from './components/WebHookList/WebHookList';

function App() {
    return (
        <WebHookList />
    );
}

export default App;
